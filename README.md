Module npm sesatheque-client
============================

Ce module propose des méthodes pour interroger une sesatheque et éventuellement normaliser ce qui en sort 
(pour toujours récupérer des sesathequeItem, avec les $displayUrl renseignés par ex, que ce soit des 
ressources ou des enfants d'arbre ou de séries).

Cf [liste des méthodes](sesatheque-client.html) (méthode du client initialisé comme ci-dessous)

Retour à la [doc générale](../../)

Pour obtenir un client, plusieurs cas de figure

via npm
-------

Votre projet dispose de npm, vous pouvez alors lancer un `npm install git://src.sesamath.net:sesatheque-client --save`
(ou ajouter `"sesatheque-client": "git://src.sesamath.net/sesatheque-client#master"` dans les dépendances de votre 
package.json suivi d'un `npm install`)
puis l'utiliser dans votre code js avec

```javascript
var stclient = require('sesatheque-client')

// cas avec une seule sésathèque, pour un usage coté serveur (installer le module https://www.npmjs.com/package/xmlhttprequest)
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest
var client = stclient({nom1: 'http://sesathequeDomain/'}, 'http://monDomaine/', XMLHttpRequest)

// pour un usage dans un navigateur
var client = stclient({nom1: 'http://sesathequeDomain/'})
client.getItem('nom1', 42, function (error, item) {
  if (error) console.error(error)
  if (item) console.log('item récupéré', item)
})

// cas avec plusieurs sésathèques
var sesatheques = {
  nom1: 'https://sesathequeQcq/',
  nom2: 'http://sesathequeQcqBis/'
}
var client = stclient(sesatheques)
client.getItem('nom2', 'titi/toto', function (error, item) {
  if (error) console.error(error)
  if (item) console.log('item récupéré', item)
})
```

via un tag script
-----------------

Dans la page html

```html
<script type="application/javascript" src="http://sesatheque/client.bundle.js"></script>
<script type="application/javascript">
  // cas avec une seule sésathèque (pas nécéssairement celle sur laquelle on a récupéré le script 
  // mais c'est plus fiable pour éviter un pb de version)
  var client = window.stclient({nom1: 'http://sesathequeDomain/'})
  client.getItem('nom1', 42, function (error, item) {
    if (error) console.error(error)
    if (item) console.log('item récupéré', item)
  })
  // cas avec plusieurs sésathèques idem précédemment
</script>
```

Développement
-------------

Pour utiliser `npm run lint`, il faut installer en global les paquets `eslint eslint-config-standard eslint-plugin-promise eslint-plugin-standard`
