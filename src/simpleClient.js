'use strict'

import { callApiUrl, getDataUrl } from './internals'
import ClientItem from './constructors/ClientItem'
import Ressource from './constructors/Ressource'

/**
 * Fournit des méthodes pour récupérer des données publiques d'une bibliotheque, sous forme d'items normalisés (avec des url absolues)
 * Initialiser le client avec {@link .getClient} puis utiliser dessus les autres méthodes
 * @service sesathequeSimpleClient
 */

/**
 * Retourne une Promise qui résoud avec les enfants de l'item (Array de ClientItem)
 * @param {ClientItem} item
 * @return {Promise<ClientItem[]>}
 */
export function fetchEnfants (item) {
  const fetchOne = (enfant) => {
    if (typeof enfant === 'string') return fetchPublicItem(enfant)
    if (enfant instanceof ClientItem) return Promise.resolve(enfant)
    return Promise.resolve(new ClientItem(enfant))
  }

  return new Promise((resolve, reject) => {
    if (item.type !== 'arbre') {
      const error = new Error('impossible de récupérer des enfants sur autre chose qu’un arbre')
      error.clientItem = item
      reject(error)
      return
    }
    if (item.enfants && item.enfants.length) {
      Promise.all(item.enfants.map(fetchOne)).then(resolve).catch(reject)
      return
    }
    // pas d'enfants, on va les chercher
    if (!item.$dataUrl) return resolve([]) // ça peut être normal, pour un dossier vide

    callApiUrl(item.$dataUrl, (error, arbre) => {
      if (error) return reject(error)
      if (arbre && arbre.type !== 'arbre') return reject(new Error('item avec $dataUrl qui ne pointe pas sur un arbre'))
      if (!arbre.enfants || !arbre.enfants.length) return resolve([])
      const items = arbre.enfants.map((enfant) => new ClientItem(enfant))
      resolve(items)
    })
  })
}

/**
 * Récupère un item sur une sésathèque (via Promise)
 * @param {string}       rid     L'identifiant unique de la ressource (baseId/origine/idOrigine également autorisé ici)
 * @param {boolean} [isPublic] passer true pour forcer public (sinon c'est privé si on a un token pour cette sésathèque)
 * @return {Promise<ClientItem>}
 */
export function fetchPublicItem (rid) {
  return new Promise((resolve, reject) => {
    const url = getDataUrl(rid, { isRef: true })
    callApiUrl(url, (error, ref) => {
      if (error) return reject(error)
      if (ref) return resolve(new ClientItem(ref))
      reject(new Error(`${url} ne renvoie ni erreur ni ressource`))
    })
  })
}

/**
 * Récupère un item sur une sésathèque (via Promise)
 * @param {string} rid L'identifiant unique de la ressource (format sesabibli/id ou baseId/origine/idOrigine)
 * @return {Promise<Ressource>}
 */
export function fetchPublicRessource (rid) {
  return new Promise((resolve, reject) => {
    const url = getDataUrl(rid)
    callApiUrl(url, (error, ressource) => {
      if (error) return reject(error)
      if (ressource) return resolve(new Ressource(ressource))
      reject(new Error(`${url} ne renvoie ni erreur ni ressource`))
    })
  })
}
