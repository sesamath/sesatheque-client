/**
 * Module léger pour la construction des différentes urls en fonction de la ressource
 * @fileOverview
 */

import tools from 'sesajstools'
import { getBaseUrl, getRidComponents } from './sesatheques'

const { hasProp } = tools

/**
 * Retourne les urls d'une ressource ou d'une ref (en tenant compte de _droits si ça existe)
 * Une url vide signifie qu'on sait que le user courant aurait un denied dessus (n'arrive pas si _droits n'existe pas)
 * @param {Ressource|Ref} ressource
 * @param {string} [myBaseUrl] Si fourni, les urls locales à cette sésathèque seront relatives
 * @return {{baseUrl: string, dataUrl: string, describeUrl: string, displayUrl: string, editUrl: string}}
 */
export default function getUrls (ressource, myBaseUrl) {
  const aliasOf = ressource.aliasOf || ressource.rid
  const isRessourceAlias = ressource.aliasOf && ressource.rid
  if (!aliasOf) throw Error('ressource invalide (ni aliasOf ni rid)')
  const [baseId, oid] = getRidComponents(aliasOf)
  let baseUrl = getBaseUrl(baseId) // peut pas être undefined (ça throw)
  if (baseUrl === myBaseUrl) baseUrl = '/'
  // si c'est pas fourni on suppose qu'on aurait le droit et on fournit les url,
  // ça mettra du 401/403 si on les utilise et que c'était pas vrai
  const droits = ressource._droits || 'RW'
  const props = {
    dataUrl: '',
    describeUrl: '',
    displayUrl: '',
    editUrl: '',
    moodleUrl: ''
  }
  const isPublic = hasProp(ressource, 'public') ? ressource.public : !ressource.restriction

  if (droits.includes('R')) {
    // dataUrl & displayUrl
    if (isPublic) {
      props.dataUrl = `${baseUrl}api/public/${oid}`
      props.displayUrl = `${baseUrl}public/voir/${oid}`
      // seules les ressources publiques ont un export moodle
      props.moodleUrl = `${baseUrl}export/public/moodle/${oid}`
    } else if (ressource.cle) {
      props.dataUrl = `${baseUrl}api/public/cle/${ressource.cle}`
      props.displayUrl = `${baseUrl}public/voir/cle/${ressource.cle}`
    } else {
      props.dataUrl = `${baseUrl}api/ressource/${oid}`
      props.displayUrl = `${baseUrl}ressource/voir/${oid}`
    }
    // describeUrl
    props.describeUrl = `${baseUrl}ressource/decrire/${oid}`
  }
  // editUrl
  if (droits.includes('W')) {
    // si c'est une ressource qui est un alias, on veut éditer l'alias et pas l'original
    if (isRessourceAlias) {
      const [bid, id] = getRidComponents(ressource.rid)
      let burl = getBaseUrl(bid)
      if (burl === myBaseUrl) burl = '/'
      props.editUrl = `${burl}ressource/modifier/${id}`
    } else {
      props.editUrl = `${baseUrl}ressource/modifier/${oid}`
    }
  }

  return props
}
