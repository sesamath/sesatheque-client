/**
 * Module léger ne contenant les méthodes utiles pour récupérer des données sur une sésathèque
 * @service fetch
 */

import { getBaseUrl, getComponents } from './sesatheques'
import { callApiUrl, getDataUrl } from './internals'

// on réexporte ça qui pourrait servir
/**
 * @see sesatheques.addSesatheque
 * @method addSesatheque
 * @memberOf fetch
 */
/**
 * @see sesatheques.exists
 * @method exists
 * @memberOf fetch
 */
export { addSesatheque, addSesatheques, exists } from './sesatheques'

// @#§! de jsdoc, impossible de lui faire générer le lien !
/**
 * Ré-export de {@link internals#callApiUrl}
 * @method callApiUrl
 * @memberOf fetch
 */
export { callApiUrl }

/**
 * Limite arbitrairement haute pour les fetch qui remontent des listes
 * alors que l'appelant ne gère pas encore de pagination
 */
export const highLimit = 100

/**
 * Appellera next avec la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
export function fetchPrivateRef (baseId, id, next) {
  const url = getDataUrl(baseId, id, { isPublic: false, isRef: true })
  callApiUrl(url, next)
}

/**
 * Appellera next avec la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
export function fetchPrivateRessource (baseId, id, next) {
  const url = getDataUrl(baseId, id, { isPublic: false })
  callApiUrl(url, next)
}

/**
 * Appellera next avec la Ref voulue
 * @param {string} baseId
 * @param {string} id
 * @param {refCallback} next sera appelé avec (error, ref)
 */
export function fetchPublicRef (baseId, id, next) {
  const url = getDataUrl(baseId, id, { isPublic: true, isRef: true })
  callApiUrl(url, next)
}

/**
 * Appellera next avec la Ressource publique voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
export function fetchPublicRessource (baseId, id, next) {
  const url = getDataUrl(baseId, id, { isPublic: true })
  callApiUrl(url, next)
}

/**
 * Appellera next avec la Ref voulue, préférer fetchPublicRef si on sait la ref publique
 * (et ce sera fetchPublicRef qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} [baseId] facultatif si id est un rid (voire un baseId/origine/idOrigine)
 * @param {string} id
 * @param {refCallback} next sera appelé avec (error, ref)
 */
export function fetchRef (baseId, id, next) {
  if (typeof id === 'function') {
    next = id
    id = baseId
    baseId = undefined
  }
  const url = getDataUrl(baseId, id, { isRef: true })
  callApiUrl(url, next)
}

/**
 * Appellera next avec la Ressource voulue, préférer fetchPublicRessource si on sait la ref publique
 * (et ce sera fetchPublicRessource qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} [baseId] facultatif si id est un rid (voire un baseId/origine/idOrigine)
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
export function fetchRessource (baseId, id, next) {
  if (typeof id === 'function') {
    next = id
    id = baseId
    baseId = undefined
  }
  const url = getDataUrl(baseId, id, { isRef: false })
  callApiUrl(url, next)
}

/**
 * Idem client.getListePerso, mais en utilisant les cookies sur le domaine courant, à n'utiliser que sur une page de la sésathèque concernée
 * (construit pour editgraphe)
 * @param {Object} [options]
 * @param {number} [options.skip]
 * @param {number} [options.limit]
 * @param {function} next
 */
export function fetchPersoOnCurrent (options, next) {
  function getList (url, liste = []) {
    // le getUrl de client n'est pas utilisable ici car on a pas de baseId,
    // on est sur une url /truc
    callApiUrl(url, { responseType: 'json', withCredentials: true }, function (error, response) {
      if (error) return next(error)
      if (!response.liste) return next(Error('Le serveur n’a pas renvoyé la réponse attendue'))
      liste = liste.concat(response.liste)
      if (liste.length > globalLimit) liste = liste.slice(0, globalLimit)
      else if (liste.length < globalLimit && response.nextUrl) return getList(response.nextUrl, liste)
      // on a fini
      sendList(liste)
    })
  }

  function sendList (liste) {
    const currentUrl = window.parent.location.href
    if (
      typeof window === 'undefined' ||
      window.parent === window ||
      !/\/ressource\/modifier\//.test(currentUrl)
    ) {
      return next(null, liste)
    }

    // on est en iframe sur de l'édition, faut filtrer la ressource courante de la liste
    // (pour l'édition j3p qui appelle cette fonction depuis une iframe de la sesathèque)
    return next(null, liste.filter(item => {
      if (!item.aliasOf) return true
      const [baseId, id] = getComponents(item.aliasOf)
      const urlModif = getBaseUrl(baseId) + 'ressource/modifier/' + id
      return currentUrl.indexOf(urlModif) !== 0
    }))
  }

  if (typeof options === 'function') {
    next = options
    options = {}
  }
  const initialSkip = (Number.isInteger(options.skip) && options.skip > 0) ? options.skip : 0
  const globalLimit = (Number.isInteger(options.limit) && options.limit > 0) ? options.limit : highLimit
  const limit = Math.min(50, globalLimit)
  getList(`${document.location.origin}/api/liste/perso?limit=${limit}&skip=${initialSkip}&orderBy=dateMiseAJour&order=desc`)
}
