/**
 * Module léger ne contenant les méthodes utiles pour récupérer des données sur une sésathèque
 * @service fetch
 */

import { getBaseUrl, getComponents } from '../sesatheques'
import { callApiUrl, getDataUrl } from './internals'

/**
 * Limite arbitrairement haute pour les fetch qui remontent des listes
 * alors que l'appelant ne gère pas encore de pagination
 */
export const highLimit = 100

/**
 * Retourne la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ref>}
 */
export function fetchPrivateRef (baseId, id) {
  const url = getDataUrl(baseId, id, { isPublic: false, isRef: true })
  return callApiUrl(url)
}

/**
 * Retourne la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ressource>}
 */
export function fetchPrivateRessource (baseId, id) {
  const url = getDataUrl(baseId, id, { isPublic: false })
  return callApiUrl(url)
}

/**
 * Retourne la Ref voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ref>}
 */
export function fetchPublicRef (baseId, id) {
  const url = getDataUrl(baseId, id, { isPublic: true, isRef: true })
  return callApiUrl(url)
}

/**
 * Appellera next avec la Ressource publique voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ressource>}
 */
export function fetchPublicRessource (baseId, id) {
  const url = getDataUrl(baseId, id, { isPublic: true })
  return callApiUrl(url)
}

/**
 * Appellera next avec la Ref voulue, préférer fetchPublicRef si on sait la ref publique
 * (et ce sera fetchPublicRef qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} baseId peut aussi être un rid
 * @param {string} [id] facultatif si baseId est un rid
 * @return {Promise<Ref>}
 */
export function fetchRef (baseId, id) {
  if (id == null && baseId.includes('/')) [baseId, id] = baseId.split('/', 2)
  const url = getDataUrl(baseId, id, { isRef: true })
  return callApiUrl(url)
}

/**
 * Appellera next avec la Ressource voulue, préférer fetchPublicRessource si on sait la ref publique
 * (et ce sera fetchPublicRessource qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} baseId peut aussi être un rid
 * @param {string} [id] facultatif si baseId est un rid
 * @return {Promise<Ressource>}
 */
export function fetchRessource (baseId, id) {
  if (id == null && baseId.includes('/')) [baseId, id] = baseId.split('/', 2)
  const url = getDataUrl(baseId, id, { isRef: false })
  return callApiUrl(url)
}

/**
 * Idem client.getListePerso, mais en utilisant les cookies sur le domaine courant, à n'utiliser que sur une page de la sésathèque concernée
 * (construit pour editgraphe)
 * @param {Object} [options]
 * @param {number} [options.skip]
 * @param {number} [options.limit]
 * @return {Promise<Ref[]>}
 */
export async function fetchPersoOnCurrent ({ skip = 0, limit = highLimit } = {}) {
  // pour gérer la pagination et rappeler plusieurs fois l'url si besoin
  async function getList (url, liste = []) {
    // le getUrl de client n'est pas utilisable ici car on a pas de baseId,
    // on est sur une url /truc
    const response = await callApiUrl(url, { responseType: 'json', withCredentials: true })
    liste = liste.concat(response.liste)
    if (liste.length < limit && response.nextUrl) return getList(response.nextUrl, liste)
    if (liste.length > limit) liste = liste.slice(0, limit)
    // on a presque fini
    const currentUrl = window.parent.location.href
    if (
      typeof window === 'undefined' ||
      window.parent === window ||
      !/\/ressource\/modifier\//.test(currentUrl)
    ) {
      return liste
    }
    // on est en iframe sur de l'édition, faut filtrer la ressource courante de la liste
    // (pour l'édition j3p qui appelle cette fonction depuis une iframe de la sesathèque)
    return liste.filter(item => {
      if (!item.aliasOf) return true
      const [baseId, id] = getComponents(item.aliasOf)
      const urlModif = getBaseUrl(baseId) + 'ressource/modifier/' + id
      return currentUrl.indexOf(urlModif) !== 0
    })
  }

  if (!Number.isInteger(skip) || skip < 0) {
    console.error(Error(`option skip invalide (${skip}), => 0`))
    skip = 0
  }
  if (!Number.isInteger(limit) || limit > highLimit) {
    console.error(Error(`option limit invalide (${limit}) => ${highLimit}`))
    limit = highLimit
  }
  return getList(`${document.location.origin}/api/liste/perso?limit=${limit}&skip=${skip}&orderBy=dateMiseAJour&order=desc`)
}
