/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de l'application Sésathèque, créée par l'association Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

'use strict'

import Ressource from './Ressource'

/**
 * Objet Serie present dans les sous-séquence, passé à saveSerie
 * @extends Ressource
 */
class Serie extends Ressource {
  /**
   * Format attendu des paramètres
   * @param {object} parametres Les paramètres spécifiques à une serie
   * @param {Ref[]} parametres.serie La liste d'exercices
   * @param {number} [parametres.minimumReussite] Minimun requis par défaut pour cette série (sera appliqué à tous ses exos qui ne le précisent pas), nombre entre 0 et 1
   * @param {number} [parametres.nonZapping] Délai en secondes de non-zapping par défaut pour cette série (sera appliqué à tous ses exos qui ne le précisent pas),
   * @param {number} [parametres.maximumVisionnage] maximumVisionnage par défaut pour cette série (sera appliqué à tous ses exos qui ne le précisent pas),
   */
  constructor (values) {
    super(values || {})
    if (!this.parametres.serie) {
      /**
       * @property parametres.serie Un tableau de Refs dont chacune peut être surchargée des propriétés minimumReussite, nonZapping, maximumVisionnage
       * @type {Ref[]}
       */
      this.parametres.serie = []
    }
    let { parametres } = values
    if (!parametres) parametres = {}
    if (parametres.minimumReussite) {
      /**
       * @property {number} parametres.minimumReussite entre 0 et 1
       * @default undefined
       */
      this.parametres.minimumReussite = parametres.minimumReussite
    }
    if (parametres.nonZapping) {
      /**
       * @property {number} parametres.nonZapping en s
       * @default undefined
       */
      this.parametres.nonZapping = parametres.nonZapping
    }
    if (parametres.maximumVisionnage) {
      /**
       * @property {number} parametres.maximumVisionnage nombre de visionnages max
       * @default undefined
       */
      this.parametres.maximumVisionnage = parametres.maximumVisionnage
    }
  }
}

export default Serie
