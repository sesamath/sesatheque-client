/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

/**
 * @service resultatFormatters
 */
/**
 * @typedef FormatterOptions
 * @type Object
 * @property {boolean} [isDaltonien=false] Passer true pour avoir la version avec icones pour daltoniens quand ça existe.
 * @property {boolean} [isTxt] Passer true pour récupérer des \n plutôt que des <br />
 */
/**
 * @callback resultatFormatter
 * @param {Resultat} resultat
 * @param {FormatterOptions} [options]
 * @returns {string}
 */
/**
 * Un formateur de résultat, lorsqu'un type d'exo veut présenter ses résultats
 * autrement qu'avec l'affichage par défaut de sesalab (de sesalab-eleve/source/client/services/formatter/index.js)
 * @typedef TypeFormatters
 * @type Object
 * @property {resultatFormatter} getHtmlFullReponse Pour une réponse en pleine page (lien détail à coté de la réponse html classique quand cette méthode existe)
 * @property {resultatFormatter} getHtmlReponse
 * @property {resultatFormatter} getHtmlScore
 * @property {resultatFormatter} getTxtReponse
 */
// le formattage par défaut (pour toutes les ressources qui n'envoient pas de résultat, simplement vu avec la durée)
import simpleFormatter from './simpleFormatter'
import { defaultFormatter } from './defaultFormatter'
import { em } from './em'
import { iep } from './iep'
import { j3p } from './j3p'
import { mathgraph } from './mathgraph'
import { url } from './url'

// les exports nommés
export const am = simpleFormatter
export const arbre = simpleFormatter // lui ne devrait jamais être utilisé, mais par cohérence on met tous nos types
export const ato = simpleFormatter
export const coll_doc = simpleFormatter // eslint-disable-line camelcase
export const ecjs = defaultFormatter
export { em }
export { iep }
export { j3p }
export { mathgraph }
export const qcm = defaultFormatter
export { url }

// et un export par défaut pour celui qui ne connait pas d'avance les type dispos et veux tous ceux qui sont gérés
export default {
  am,
  arbre,
  ato,
  // eslint-disable-next-line camelcase
  coll_doc,
  ecjs,
  em,
  iep,
  j3p,
  mathgraph,
  qcm,
  url
}
