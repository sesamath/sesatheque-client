/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
/**
 * Au delà on affiche le lien détail
 * @type {number}
 */
const shortResponseLimit = 200

const getReponse = (resultat) => {
  let output
  // pour url on a pas de resultat.reponse, seule la durée peut servir
  if (resultat.duree > 0) {
    output = `${resultat.reponse ? 'répondu après' : 'affiché pendant'} `
    if (resultat.duree > 59) {
      output += Math.floor(resultat.duree / 60) + ' minutes '
    }
    output += resultat.duree % 60 + ' s'
  } else {
    output = "pas de durée d'affichage connue"
  }

  return output
}
const hasLongResponse = (resultat) => resultat.reponse && resultat.reponse.length > shortResponseLimit
const hasShortResponse = (resultat) => resultat.reponse && resultat.reponse.length <= shortResponseLimit

const nl2br = (s) => typeof s === 'string' ? s.replace(/\n/g, '<br>') : s

/**
 * Aide mathenpoche (score toujours 'vu', la durée comme réponse)
 * @type TypeFormatters
 * @memberOf resultatFormatters
 */
export const url = {
  /**
   * Retourne le score à afficher sur la page html des bilans (ici toujours vu)
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlScore: (resultat) => resultat.reponse ? 'répondu' : 'vu',

  /**
   * Retourne la réponse à insérer sur la page html des bilans (ici la durée d'affichage)
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlReponse: (resultat) => {
    const txtReponse = getReponse(resultat)
    if (!hasShortResponse(resultat)) return txtReponse
    return txtReponse + '<br>Réponse :<br>' + nl2br(resultat.reponse)
  },
  /**
   * Retourne la réponse à insérer dans les exports
   * @param {Resultat} resultat
   * @return {string}
   */
  getTxtReponse: (resultat) => resultat.reponse || getReponse(resultat),
  /**
   * Réponse en pleine page
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlFullReponse: (resultat) => {
    if (!hasLongResponse(resultat)) return '' // y'aura pas de lien détails
    return `<p class="resultatDetails">${nl2br(resultat.reponse)}</p>`
  }
}
