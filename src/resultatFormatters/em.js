/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

import { getBaseUrlFromResultat } from './helper'

/**
 * Récupère des résultats incohérents
 * @private
 * @param {Resultat} resultat
 * @return {Resultat} avec ajout de reponse si absent mais présent dans original
 */
const checkEmResultat = (resultat) => {
  if (resultat && !resultat.reponse && resultat.original && resultat.original.reponse) {
    // vraiment pas normal
    const error = Error('résultat em incohérent (reponse vide alors qu’elle existe dans original)')
    // on en a souvent, on arrête de polluer bugsnag avec ça
    // if (typeof window !== 'undefined' && window.bugsnagClient) window.bugsnagClient.notify(error, {severity: 'error', metaData: {resultat}})
    console.error(error)
    resultat.reponse = resultat.original.reponse
  }
  return resultat
}

/**
 * Exo mathenpoche en flash, reponse genre vvrbjv affiché sous forme de petits rectangles colorés)
 * @type TypeFormatters
 * @memberOf resultatFormatters
 */
export const em = {
  /**
   * Retourne le score à afficher sur la page html des bilans (pas forcément un nombre)
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlScore: (resultat) => {
    let output = ''
    let nbok = 0
    let nbq
    let lettre
    resultat = checkEmResultat(resultat)
    // pour em on s'attend à avoir resultat.reponse sous la forme d'une chaine vvprbb
    // mais la longueur de la chaine ne correspond qu'aux questions répondues
    if (typeof resultat.reponse === 'string') {
      nbq = resultat.reponse.length
      for (let i = 0; i < nbq; i++) {
        lettre = resultat.reponse[i]
        if (lettre === 'v' || lettre === 'p') nbok++
      }
      output = nbok + ' / ' + nbq
    } else {
      output = 'pas de réponse ou réponse à un mauvais format'
    }

    return output
  },

  /**
   * Retourne la réponse à insérer sur la page html des bilans
   * @param {Resultat} resultat
   * @param {FormatterOptions} [options]
   * @return {string}
   */
  getHtmlReponse: (resultat, options) => {
    if (!options) options = {}
    let output = ''
    resultat = checkEmResultat(resultat)
    // pour em on s'attend à avoir resultat.reponse sous la forme d'une chaine vvprbb
    // mais la longueur de la chaine ne correspond qu'aux questions répondues
    if (typeof resultat.reponse === 'string') {
      const baseUrl = getBaseUrlFromResultat(resultat)
      for (const lettre of resultat.reponse) {
        output += `<img src="${baseUrl}plugins/em/images/reponse_`
        if (options.isDaltonien) output += 'h_'
        output += `${lettre}.gif" width="10" height="15" alt="">`
      }
    } else {
      output = 'pas de réponse ou réponse à un mauvais format'
    }

    return output
  }
}
/**
 * Retourne le score sous sa forme txt
 */
em.getTxtReponse = em.getHtmlScore
