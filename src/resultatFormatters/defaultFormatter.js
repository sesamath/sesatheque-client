/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
const getTxtReponse = (resultat) => typeof resultat.score === 'number' ? Math.round(resultat.score * 100) + '%' : 'inconnu'
/**
 * Exo mathenpoche en flash, reponse genre vvrbjv affiché sous forme de petits rectangles colorés)
 * @type TypeFormatters
 * @memberOf resultatFormatters
 */
export const defaultFormatter = {
  /**
   * Retourne le score à afficher sur la page html des bilans (pas forcément un nombre)
   * @param {Resultat} resultat
   * @return {string}
   */
  getHtmlScore: getTxtReponse,

  /**
   * Retourne la réponse à insérer sur la page html des bilans
   * @param {Resultat} resultat
   * @param {FormatterOptions} [options]
   * @return {string}
   */
  getHtmlReponse: (resultat) => typeof resultat.reponse === 'string' ? resultat.reponse : getTxtReponse(resultat),
  /**
   * Retourne le score sous sa forme txt
   */
  getTxtReponse
}
