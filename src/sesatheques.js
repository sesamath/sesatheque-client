'use strict'
// ce fichier ne sert qu'à faire des exports nommés à partir de l'objet sesatheques immutable
import sesatheques from './sesatheques.protected'

export default sesatheques
export const reBaseUrl = sesatheques.reBaseUrl
export const addSesatheque = sesatheques.addSesatheque
export const addSesatheques = sesatheques.addSesatheques
export const exists = sesatheques.exists
export const getBaseId = sesatheques.getBaseId
export const getBaseIdFromRessource = sesatheques.getBaseIdFromRessource
export const getBaseIdFromRid = sesatheques.getBaseIdFromRid
export const getBaseIdFromUrlQcq = sesatheques.getBaseIdFromUrlQcq
export const getBaseUrl = sesatheques.getBaseUrl
export const getComponents = sesatheques.getComponents
export const getList = sesatheques.getList
export const getRidComponents = sesatheques.getRidComponents
export const defaultBaseId = sesatheques.defaultBaseId
export const defaultBaseUrl = sesatheques.defaultBaseUrl
