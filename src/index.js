'use strict'

import log from 'sesajstools/utils/log'
import xhr from 'sesajstools/http/xhr'

import client from './client'

/**
 * Constructeur du client, qui retournera un client avec la liste des méthodes
 * @param {object|string}  sesatheques      objet {name1:url1, name2:url2,…}, ou url d'une sesatheque unique
 *                                            (dans ce cas ne pas passer l'argument baseId à chaque méthode du client récupéré)
 * @param {string}         [baseId]         La baseId du sesalab qui nous utilise (ou à défaut une baseUrl),
 *                                          Si absent on prendra le domaine courant (avec un usage dans un navigateur)
 *                                          Ne sert que pour fixer la propriété origine dans
 *                                            saveSequenceModele et saveSerie
 * @param {XMLHttpRequest} [XMLHttpRequest] Un éventuel constructeur XMLHttpRequest si on tourne coté serveur,
 *                                            à construire avec import {XMLHttpRequest} from 'xmlhttprequest'
 * @returns {sesathequeClient} Le client, liste de méthodes
 */
export default function getClient (sesatheques, baseId, XMLHttpRequest) {
  if (Array.isArray(sesatheques)) {
    client.addSesatheques(sesatheques)
  } else if (sesatheques && sesatheques.baseId && sesatheques.baseUrl) {
    client.addSesatheque(sesatheques.baseId, sesatheques.baseUrl)
  } else {
    // on accepte une baseId connue seule
    if (typeof sesatheques !== 'string') throw Error('Pour indiquer les sésathèques à utiliser, il faut passer une baseId ou un objet {baseId, baseUrl} ou un tableau qui en contient (mix autorisé)')
    if (!client.exists(sesatheques)) throw Error(`${sesatheques} n’est pas une sésathèque connue`)
  }

  client.setOrigine(baseId)

  if (typeof XMLHttpRequest === 'function') {
    log('on impose un xhr')
    xhr.setXMLHttpRequest(XMLHttpRequest)
  }

  return client
}
