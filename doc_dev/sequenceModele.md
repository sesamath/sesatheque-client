Séquences et Séquences modèles
==============================

Séquences
---------

Les séquences ont les caractéristiques suivantes

* nom 

un paramétrage (idem sous-séquence et série) avec

- description (pour les profs)
- statut (active|inactive|bornée)
- prioritaire oui|non
- message (le message à afficher aux élèves)
- type libre|ordonnée|ordonnée à minimum de réussite
- durée non zapping
- minimum de réussite
- max visionnage

et une liste de sous-séquences, chacune ayant les caractéristiques ci-dessus, avec en plus

- liste de groupes et d'élèves
- série (liste de ressources)

Elles sont gérées uniquement dans Sesalab

Pour pouvoir être partagées, il faut en retirer les élèves, on a donc la sequenceModele qui est une séquence sans élève.

