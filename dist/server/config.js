/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans lespoir quil sera utile, mais SANS AUCUNE GARANTIE ;
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou dADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict';

// attention, les niveaux sont à 3 endroits (pas top, on fera mieux la prochaine fois)
// ici, dans config.constantes.niveaux pour avoir des propriétés intelligibles
// à utiliser dans le code pour comparer les valeurs, et dans config.ordre
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.constantes = void 0;
exports.getLabel = getLabel;
exports.guessNiveau = guessNiveau;
exports.isEditable = isEditable;
exports.ordre = exports.niveaux = void 0;
const niveaux = exports.niveaux = {
  mtps: 'très petite section',
  mps: 'petite section',
  mms: 'moyenne section',
  mgs: 'grande section',
  11: 'CP',
  10: 'CE1',
  9: 'CE2',
  8: 'CM1',
  7: 'CM2',
  6: 'sixième',
  5: 'cinquième',
  4: 'quatrième',
  3: 'troisième',
  2: 'seconde',
  '2p': 'seconde pro',
  1: 'première',
  '1p': 'première pro',
  12: 'terminale',
  '12p': 'terminale pro',
  cap1: 'CAP 1re année',
  cap2: 'CAP 2e année',
  bep1: 'BEP 1re année',
  bep2: 'BEP 2e année'
};
const constantes = exports.constantes = {
  // reverse nomLisible => id pour rendre le code js plus lisible
  niveaux: {
    maternelleTps: 'mtps',
    maternellePs: 'mps',
    maternelleMs: 'mms',
    maternelleGs: 'mgs',
    cp: '11',
    ce1: '10',
    ce2: '9',
    cm1: '8',
    cm2: '7',
    '6e': '6',
    '5e': '5',
    '4e': '4',
    '3e': '3',
    '2de': '2',
    '2pro': '2p',
    '1re': '1',
    '1pro': '1p',
    tle: '12',
    tpro: '12p',
    cap1: 'cap1',
    cap2: 'cap2',
    bep1: 'bep1',
    bep2: 'bep2'
  },
  restriction: {
    aucune: 0,
    correction: 1,
    groupe: 2,
    prive: 3
  },
  routes: {
    api: '',
    display: 'voir',
    preview: 'apercevoir',
    create: 'ajouter',
    edit: 'modifier',
    describe: 'decrire',
    delete: 'supprimer',
    search: 'rechercher',
    history: 'voirHistorique'
  }
};

/**
 * Les props éditable / label de chaque type de ressource (plugin)
 * (utiliser isEditable & getLabel pour récupérer ces infos)
 * @private
 */
const options = {
  am: {
    editable: false,
    label: 'aide mathenpoche'
  },
  arbre: {
    editable: true,
    label: 'arbre'
  },
  ato: {
    editable: false,
    label: 'atome de manuel'
  },
  // calkc: { editable: true, label: 'calculatrice cassée' },
  coll_doc: {
    editable: false,
    label: 'complément de manuel'
  },
  ecjs: {
    editable: true,
    label: 'calcul@tice'
  },
  em: {
    editable: false,
    label: 'exercice mathenpoche'
  },
  iep: {
    editable: true,
    label: 'instrumenpoche'
  },
  j3p: {
    editable: true,
    label: 'exercice interactif'
  },
  mathgraph: {
    editable: true,
    label: 'mathgraph'
  },
  // mental: { editable: true, label: 'calcul mental' },
  qcm: {
    editable: true,
    label: 'qcm'
  },
  serie: {
    editable: false,
    label: 'série'
  },
  // tep: { editable: false, label: 'figure tracenpoche' },
  // testd: { editable: false, label: 'tests diagnostiques IREM de Lorraine' },
  url: {
    editable: true,
    label: 'page externe'
  }
};
const ordre = exports.ordre = {
  niveaux: ['mtps', 'mps', 'mms', 'mgs', '11', '10', '9', '8', '7', '6', '5', '4', '3', 'cap1', 'cap2', 'bep1', 'bep2', '2', '2p', '1', '1p', '12', '12p']
};

/**
 * Retourne true si la ressource peut être modifiée (i.e. un éditeur existe)
 * @param {string} type
 * @return {boolean}
 */
function isEditable(type) {
  if (!options[type]) throw Error(`Type ${type} inconnu`);
  return options[type].editable;
}
/**
 * Retourne le label pour ce type de ressource
 * @param {string} type
 * @return {string} un nom plus intelligible que le type am|em|…
 */
function getLabel(type) {
  if (!options[type]) throw Error(`Type ${type} inconnu`);
  return options[type].label;
}

/**
 * Retourne le code niveau probable d'après un nom de classe
 * @param {string} classe
 * @param {string} [forceDegre] passer primaire ou secondaire pour ne tester que ces niveaux (utile de forcer primaire pour éviter que du "tps" ne se retrouve en terminale)
 * @returns {string} Le code du niveau, undefined si on a rien deviné
 */
function guessNiveau(classe, forceDegre) {
  function guessPrimaire() {
    // primaire
    if (/^cp/.test(classe)) return n.cp;
    if (/^ce1/.test(classe)) return n.ce1;
    if (/^ce2/.test(classe)) return n.ce2;
    if (/^cm1/.test(classe)) return n.cm1;
    if (/^cm2/.test(classe)) return n.cm2;
    if (/^cours/.test(classe)) {
      if (/préparatoire/.test(classe)) return n.cp;
      if (/élémentaire.1/.test(classe)) return n.ce1;
      if (/élémentaire.2/.test(classe)) return n.ce2;
      if (/moyen.1/.test(classe)) return n.cm1;
      if (/moyen.2/.test(classe)) return n.cm2;
    }
    // maternelle
    if (/section/.test(classe)) {
      // et si en plus de "section" on trouve ces patterns (avant ou après osef)
      if (/(?:très|tout).+petit/.test(classe)) return n.maternelleTps;
      if (/petit/.test(classe)) return n.maternellePs;
      if (/moyen/.test(classe)) return n.maternelleMs;
      if (/grand/.test(classe)) return n.maternelleGs;
    }
    // au cas où certains mettrait ces abbréviations
    if (/^tps/.test(classe)) return n.maternelleTps;
    if (/^ps/.test(classe)) return n.maternellePs;
    if (/^ms/.test(classe)) return n.maternelleMs;
    if (/^gs/.test(classe)) return n.maternelleGs;
  }
  function guessSecondaire() {
    // d'abord le collège, plus simple et plus fréquent
    if (/^6/.test(classe)) return n['6e'];
    if (/^5/.test(classe)) return n['5e'];
    if (/^4/.test(classe)) return n['4e'];
    if (/^3/.test(classe)) return n['3e'];
    // lycée
    if (/^2.*pro/.test(classe)) return n['2pro'];
    if (/^2.*cap/.test(classe)) return n.cap2;
    if (/^2.*bep/.test(classe)) return n.bep2;
    if (/cap.*2/.test(classe)) return n.cap2;
    if (/bep.*2/.test(classe)) return n.bep2;
    if (/^2/.test(classe)) return n['2de'];
    if (/^1.*cap/.test(classe)) return n.cap1;
    if (/cap.*1/.test(classe)) return n.cap1;
    if (/^1.*bep/.test(classe)) return n.bep1;
    if (/bep.*1/.test(classe)) return n.bep1;
    if (/^1/.test(classe)) return n['1re'];
    if (/^t.*pro/.test(classe)) return n.tpro;
    if (/^t/.test(classe)) return n.tle;
  }
  const n = constantes.niveaux;
  // on fait ça une fois pour tous les tests, pour s'éviter le /i partout
  classe = classe.toLowerCase();
  switch (forceDegre) {
    case 'primaire':
      return guessPrimaire();
    case 'secondaire':
      return guessSecondaire();
    default:
      return guessSecondaire() || guessPrimaire();
  }
}
var _default = exports.default = {
  constantes,
  getLabel,
  guessNiveau,
  isEditable,
  niveaux
};