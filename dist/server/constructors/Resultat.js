/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de l'application Sésathèque, créée par l'association Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _sesajstools = _interopRequireDefault(require("sesajstools"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const {
  hasProp
} = _sesajstools.default;

/**
 * Définition d'un résultat commune à toutes les ressources (exercices ou pas)
 * @constructor
 * @param {object} values Un objet ayant des propriétés d'un résultat
 * @throws Error si values est truthy mais non object
 */
function Resultat(values) {
  if (!values) values = {};
  if (typeof values !== 'object') throw Error('Paramètre invalide pour un résultat');
  /**
   * Le rid / aliasOf de la ressource
   * @type {string}
   */
  this.rid = values.rid;

  /**
   * Le type de la ressource (type de la ressource, nom de code du plugin qui la gère et saura afficher le résultat)
   * @default undefined
   * @type {string}
   */
  this.type = values.type;

  /**
   * La date du résultat
   * @default new Date()
   * @type {Date}
   */
  this.date = values.date || new Date();

  /**
   * La durée en seconde entre le début de l'affichage de la ressource et l'envoi de ce résultat
   * @default null
   * @type {Integer}
   */
  this.duree = values.duree || null;

  /**
   * Vaut true quand c'est le dernier envoi de l'exercice (seulement pour certains types)
   * @default undefined
   * @type {boolean}
   */
  this.fin = values.fin;
  if (hasProp(values, 'score')) {
    /**
     * Le score numérique, entre 0 et 1
     * @default undefined
     * @type {number|undefined}
     */
    this.score = Number(values.score);
    if (Number.isNaN(this.score)) throw new TypeError('score invalide (pas un nombre)');
    if (this.score < 0) throw new TypeError('score invalide (négatif)');
    if (this.score > 1) throw new TypeError('score invalide (> 1)');
  } else {
    // pour garantir le hasOwnProperty, au cas où
    this.score = undefined;
  }

  /**
   * Le résultat sous une forme qualitative (rrvb pour mep, phrase d'état pour j3p, etc.)
   * @default ''
   * @type {string|*}
   */
  this.reponse = values.reponse || ''

  // les propriétés facultatives que l'on prend dans l'original
  /**
   * Éventuel objet initial ayant donné ce résultat
   * @property original
   * @type {Object}
   * @default undefined
   */
  /**
   * Un contenu pour une réponse qui ne rentre pas dans la string réponse
   * (sert à distinguer les résultats où le formateur peut aller consulter un objet réponse,
   * ça peut être un paragraphe de texte, un objet xml ou base64 pour certains types, etc.)
   * @property contenu
   * @default undefined
   * @type {string|*}
   */;
  ['contenu', 'oid', 'original', '$resetDelay'].forEach(prop => {
    if (hasProp(values, prop)) this[prop] = values[prop];
  });
}

/**
 * Cast en string d'un Resultat (sa reponse)
 * @returns {string}
 */
Resultat.prototype.toString = function () {
  return typeof this.reponse === 'string' ? this.reponse : this.reponse ? this.reponse.toString() : '';
};
var _default = exports.default = Resultat;