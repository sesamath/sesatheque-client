/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de l'application Sésathèque, créée par l'association Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _sesajstools = _interopRequireDefault(require("sesajstools"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const {
  hasProp
} = _sesajstools.default;

/**
 * Objet Sequence de sesalab, n'est pas utilisé par sesatheque ou sesatheque-client, mis ici pour documenter le format
 * @constructor
 */
function Sequence(values) {
  /**
   * Titre de la séquence
   * @type string
   * @default ''
   */
  this.nom = values.nom || '';
  /**
   * Liste des sous-séquences
   * @type {SousSequence[]}
   */
  this.sousSequences = values.sousSequences || [];
  /**
   * True si la séquence est prioritaire
   * @type boolean
   * @default false
   */
  this.prioritaire = Boolean(values.prioritaire);
  /**
   * True si la séquence est active
   * @type boolean
   * @default true
   */
  this.active = hasProp(values, 'active') ? Boolean(values.active) : true;
  /**
   * 0 => libre
   * 1 => ordonnée
   * 2 => ordonnée avec minimum de réussite
   * @type number
   * @default 0
   */
  this.type = values.type || 0;
  if ([0, 1, 2].indexOf(this.type) === -1) throw Error(`Type ${values.type} invalide`);
  /**
   * Description
   * @type {string}
   * @default ''
   */
  this.description = values.description || '';
  /**
   * Message pour l'élève
   * @type {string}
   * @default ''
   */
  this.message = values.message || '';
  /**
   * Timestamp de début d'une séquence limitée dans le temps
   * @type {number}
   * @default undefined
   */
  this.fromTs = values.fromTs;
  /**
   * Timestamp de fin d'une séquence limitée dans le temps
   * @type {number}
   * @default undefined
   */
  this.toTs = values.toTs;
  /**
   * Délai en s de non-zapping appliqué aux exercices qui ne le préciseraient pas
   * @type {number}
   * @default 10
   */
  this.nonZapping = values.nonZapping || 10;
  /**
   * Nb maximum de visionnage appliqué aux exercices qui ne le préciseraient pas
   * @type {number}
   * @default undefined
   */
  this.maximumVisionnage = values.maximumVisionnage;
  /**
   * Minimum de réussite appliqué aux exercices qui ne le préciseraient pas (nombre entre 0 et 1)
   * @type {number}
   * @default undefined
   */
  this.minimumReussite = values.minimumReussite;
}
var _default = exports.default = Sequence;