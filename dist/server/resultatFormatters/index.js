"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ecjs = exports.default = exports.coll_doc = exports.ato = exports.arbre = exports.am = void 0;
Object.defineProperty(exports, "em", {
  enumerable: true,
  get: function () {
    return _em.em;
  }
});
Object.defineProperty(exports, "iep", {
  enumerable: true,
  get: function () {
    return _iep.iep;
  }
});
Object.defineProperty(exports, "j3p", {
  enumerable: true,
  get: function () {
    return _j3p.j3p;
  }
});
Object.defineProperty(exports, "mathgraph", {
  enumerable: true,
  get: function () {
    return _mathgraph.mathgraph;
  }
});
exports.qcm = void 0;
Object.defineProperty(exports, "url", {
  enumerable: true,
  get: function () {
    return _url.url;
  }
});
var _simpleFormatter = _interopRequireDefault(require("./simpleFormatter"));
var _defaultFormatter = require("./defaultFormatter");
var _em = require("./em");
var _iep = require("./iep");
var _j3p = require("./j3p");
var _mathgraph = require("./mathgraph");
var _url = require("./url");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
/**
 * This file is part of Sesatheque.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesatheque is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesatheque is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesatheque (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de lapplication Sésathèque, créée par lassociation Sésamath.
 *
 * Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

/**
 * @service resultatFormatters
 */
/**
 * @typedef FormatterOptions
 * @type Object
 * @property {boolean} [isDaltonien=false] Passer true pour avoir la version avec icones pour daltoniens quand ça existe.
 * @property {boolean} [isTxt] Passer true pour récupérer des \n plutôt que des <br />
 */
/**
 * @callback resultatFormatter
 * @param {Resultat} resultat
 * @param {FormatterOptions} [options]
 * @returns {string}
 */
/**
 * Un formateur de résultat, lorsqu'un type d'exo veut présenter ses résultats
 * autrement qu'avec l'affichage par défaut de sesalab (de sesalab-eleve/source/client/services/formatter/index.js)
 * @typedef TypeFormatters
 * @type Object
 * @property {resultatFormatter} getHtmlFullReponse Pour une réponse en pleine page (lien détail à coté de la réponse html classique quand cette méthode existe)
 * @property {resultatFormatter} getHtmlReponse
 * @property {resultatFormatter} getHtmlScore
 * @property {resultatFormatter} getTxtReponse
 */
// le formattage par défaut (pour toutes les ressources qui n'envoient pas de résultat, simplement vu avec la durée)

// les exports nommés
const am = exports.am = _simpleFormatter.default;
const arbre = exports.arbre = _simpleFormatter.default; // lui ne devrait jamais être utilisé, mais par cohérence on met tous nos types
const ato = exports.ato = _simpleFormatter.default;
const coll_doc = exports.coll_doc = _simpleFormatter.default; // eslint-disable-line camelcase
const ecjs = exports.ecjs = _defaultFormatter.defaultFormatter;
const qcm = exports.qcm = _defaultFormatter.defaultFormatter;
// et un export par défaut pour celui qui ne connait pas d'avance les type dispos et veux tous ceux qui sont gérés
var _default = exports.default = {
  am,
  arbre,
  ato,
  // eslint-disable-next-line camelcase
  coll_doc,
  ecjs,
  em: _em.em,
  iep: _iep.iep,
  j3p: _j3p.j3p,
  mathgraph: _mathgraph.mathgraph,
  qcm,
  url: _url.url
};