"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "addSesatheque", {
  enumerable: true,
  get: function () {
    return _sesatheques.addSesatheque;
  }
});
Object.defineProperty(exports, "addSesatheques", {
  enumerable: true,
  get: function () {
    return _sesatheques.addSesatheques;
  }
});
Object.defineProperty(exports, "callApiUrl", {
  enumerable: true,
  get: function () {
    return _internals.callApiUrl;
  }
});
Object.defineProperty(exports, "exists", {
  enumerable: true,
  get: function () {
    return _sesatheques.exists;
  }
});
exports.fetchPersoOnCurrent = fetchPersoOnCurrent;
exports.fetchPrivateRef = fetchPrivateRef;
exports.fetchPrivateRessource = fetchPrivateRessource;
exports.fetchPublicRef = fetchPublicRef;
exports.fetchPublicRessource = fetchPublicRessource;
exports.fetchRef = fetchRef;
exports.fetchRessource = fetchRessource;
exports.highLimit = void 0;
var _sesatheques = require("./sesatheques");
var _internals = require("./internals");
/**
 * Module léger ne contenant les méthodes utiles pour récupérer des données sur une sésathèque
 * @service fetch
 */

// on réexporte ça qui pourrait servir
/**
 * @see sesatheques.addSesatheque
 * @method addSesatheque
 * @memberOf fetch
 */
/**
 * @see sesatheques.exists
 * @method exists
 * @memberOf fetch
 */

// @#§! de jsdoc, impossible de lui faire générer le lien !
/**
 * Ré-export de {@link internals#callApiUrl}
 * @method callApiUrl
 * @memberOf fetch
 */

/**
 * Limite arbitrairement haute pour les fetch qui remontent des listes
 * alors que l'appelant ne gère pas encore de pagination
 */
const highLimit = exports.highLimit = 100;

/**
 * Appellera next avec la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
function fetchPrivateRef(baseId, id, next) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: false,
    isRef: true
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Appellera next avec la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
function fetchPrivateRessource(baseId, id, next) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: false
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Appellera next avec la Ref voulue
 * @param {string} baseId
 * @param {string} id
 * @param {refCallback} next sera appelé avec (error, ref)
 */
function fetchPublicRef(baseId, id, next) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: true,
    isRef: true
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Appellera next avec la Ressource publique voulue
 * @param {string} baseId
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
function fetchPublicRessource(baseId, id, next) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: true
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Appellera next avec la Ref voulue, préférer fetchPublicRef si on sait la ref publique
 * (et ce sera fetchPublicRef qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} [baseId] facultatif si id est un rid (voire un baseId/origine/idOrigine)
 * @param {string} id
 * @param {refCallback} next sera appelé avec (error, ref)
 */
function fetchRef(baseId, id, next) {
  if (typeof id === 'function') {
    next = id;
    id = baseId;
    baseId = undefined;
  }
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isRef: true
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Appellera next avec la Ressource voulue, préférer fetchPublicRessource si on sait la ref publique
 * (et ce sera fetchPublicRessource qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} [baseId] facultatif si id est un rid (voire un baseId/origine/idOrigine)
 * @param {string} id
 * @param {ressourceCallback} next sera appelé avec (error, ressource)
 */
function fetchRessource(baseId, id, next) {
  if (typeof id === 'function') {
    next = id;
    id = baseId;
    baseId = undefined;
  }
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isRef: false
  });
  (0, _internals.callApiUrl)(url, next);
}

/**
 * Idem client.getListePerso, mais en utilisant les cookies sur le domaine courant, à n'utiliser que sur une page de la sésathèque concernée
 * (construit pour editgraphe)
 * @param {Object} [options]
 * @param {number} [options.skip]
 * @param {number} [options.limit]
 * @param {function} next
 */
function fetchPersoOnCurrent(options, next) {
  function getList(url, liste = []) {
    // le getUrl de client n'est pas utilisable ici car on a pas de baseId,
    // on est sur une url /truc
    (0, _internals.callApiUrl)(url, {
      responseType: 'json',
      withCredentials: true
    }, function (error, response) {
      if (error) return next(error);
      if (!response.liste) return next(Error('Le serveur n’a pas renvoyé la réponse attendue'));
      liste = liste.concat(response.liste);
      if (liste.length > globalLimit) liste = liste.slice(0, globalLimit);else if (liste.length < globalLimit && response.nextUrl) return getList(response.nextUrl, liste);
      // on a fini
      sendList(liste);
    });
  }
  function sendList(liste) {
    const currentUrl = window.parent.location.href;
    if (typeof window === 'undefined' || window.parent === window || !/\/ressource\/modifier\//.test(currentUrl)) {
      return next(null, liste);
    }

    // on est en iframe sur de l'édition, faut filtrer la ressource courante de la liste
    // (pour l'édition j3p qui appelle cette fonction depuis une iframe de la sesathèque)
    return next(null, liste.filter(item => {
      if (!item.aliasOf) return true;
      const [baseId, id] = (0, _sesatheques.getComponents)(item.aliasOf);
      const urlModif = (0, _sesatheques.getBaseUrl)(baseId) + 'ressource/modifier/' + id;
      return currentUrl.indexOf(urlModif) !== 0;
    }));
  }
  if (typeof options === 'function') {
    next = options;
    options = {};
  }
  const initialSkip = Number.isInteger(options.skip) && options.skip > 0 ? options.skip : 0;
  const globalLimit = Number.isInteger(options.limit) && options.limit > 0 ? options.limit : highLimit;
  const limit = Math.min(50, globalLimit);
  getList(`${document.location.origin}/api/liste/perso?limit=${limit}&skip=${initialSkip}&orderBy=dateMiseAJour&order=desc`);
}