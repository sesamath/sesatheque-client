"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchPersoOnCurrent = fetchPersoOnCurrent;
exports.fetchPrivateRef = fetchPrivateRef;
exports.fetchPrivateRessource = fetchPrivateRessource;
exports.fetchPublicRef = fetchPublicRef;
exports.fetchPublicRessource = fetchPublicRessource;
exports.fetchRef = fetchRef;
exports.fetchRessource = fetchRessource;
exports.highLimit = void 0;
var _sesatheques = require("../sesatheques");
var _internals = require("./internals");
/**
 * Module léger ne contenant les méthodes utiles pour récupérer des données sur une sésathèque
 * @service fetch
 */

/**
 * Limite arbitrairement haute pour les fetch qui remontent des listes
 * alors que l'appelant ne gère pas encore de pagination
 */
const highLimit = exports.highLimit = 100;

/**
 * Retourne la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ref>}
 */
function fetchPrivateRef(baseId, id) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: false,
    isRef: true
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Retourne la Ressource voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ressource>}
 */
function fetchPrivateRessource(baseId, id) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: false
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Retourne la Ref voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ref>}
 */
function fetchPublicRef(baseId, id) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: true,
    isRef: true
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Appellera next avec la Ressource publique voulue
 * @param {string} baseId
 * @param {string} id
 * @return {Promise<Ressource>}
 */
function fetchPublicRessource(baseId, id) {
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isPublic: true
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Appellera next avec la Ref voulue, préférer fetchPublicRef si on sait la ref publique
 * (et ce sera fetchPublicRef qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} baseId peut aussi être un rid
 * @param {string} [id] facultatif si baseId est un rid
 * @return {Promise<Ref>}
 */
function fetchRef(baseId, id) {
  if (id == null && baseId.includes('/')) [baseId, id] = baseId.split('/', 2);
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isRef: true
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Appellera next avec la Ressource voulue, préférer fetchPublicRessource si on sait la ref publique
 * (et ce sera fetchPublicRessource qui sera appelé si on a pas de token pour cette baseId)
 * @param {string} baseId peut aussi être un rid
 * @param {string} [id] facultatif si baseId est un rid
 * @return {Promise<Ressource>}
 */
function fetchRessource(baseId, id) {
  if (id == null && baseId.includes('/')) [baseId, id] = baseId.split('/', 2);
  const url = (0, _internals.getDataUrl)(baseId, id, {
    isRef: false
  });
  return (0, _internals.callApiUrl)(url);
}

/**
 * Idem client.getListePerso, mais en utilisant les cookies sur le domaine courant, à n'utiliser que sur une page de la sésathèque concernée
 * (construit pour editgraphe)
 * @param {Object} [options]
 * @param {number} [options.skip]
 * @param {number} [options.limit]
 * @return {Promise<Ref[]>}
 */
async function fetchPersoOnCurrent({
  skip = 0,
  limit = highLimit
} = {}) {
  // pour gérer la pagination et rappeler plusieurs fois l'url si besoin
  async function getList(url, liste = []) {
    // le getUrl de client n'est pas utilisable ici car on a pas de baseId,
    // on est sur une url /truc
    const response = await (0, _internals.callApiUrl)(url, {
      responseType: 'json',
      withCredentials: true
    });
    liste = liste.concat(response.liste);
    if (liste.length < limit && response.nextUrl) return getList(response.nextUrl, liste);
    if (liste.length > limit) liste = liste.slice(0, limit);
    // on a presque fini
    const currentUrl = window.parent.location.href;
    if (typeof window === 'undefined' || window.parent === window || !/\/ressource\/modifier\//.test(currentUrl)) {
      return liste;
    }
    // on est en iframe sur de l'édition, faut filtrer la ressource courante de la liste
    // (pour l'édition j3p qui appelle cette fonction depuis une iframe de la sesathèque)
    return liste.filter(item => {
      if (!item.aliasOf) return true;
      const [baseId, id] = (0, _sesatheques.getComponents)(item.aliasOf);
      const urlModif = (0, _sesatheques.getBaseUrl)(baseId) + 'ressource/modifier/' + id;
      return currentUrl.indexOf(urlModif) !== 0;
    });
  }
  if (!Number.isInteger(skip) || skip < 0) {
    console.error(Error(`option skip invalide (${skip}), => 0`));
    skip = 0;
  }
  if (!Number.isInteger(limit) || limit > highLimit) {
    console.error(Error(`option limit invalide (${limit}) => ${highLimit}`));
    limit = highLimit;
  }
  return getList(`${document.location.origin}/api/liste/perso?limit=${limit}&skip=${skip}&orderBy=dateMiseAJour&order=desc`);
}