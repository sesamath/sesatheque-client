'use strict';

// ce fichier ne sert qu'à faire des exports nommés à partir de l'objet sesatheques immutable
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reBaseUrl = exports.getRidComponents = exports.getList = exports.getComponents = exports.getBaseUrl = exports.getBaseIdFromUrlQcq = exports.getBaseIdFromRid = exports.getBaseIdFromRessource = exports.getBaseId = exports.exists = exports.defaultBaseUrl = exports.defaultBaseId = exports.default = exports.addSesatheques = exports.addSesatheque = void 0;
var _sesatheques = _interopRequireDefault(require("./sesatheques.protected"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _default = exports.default = _sesatheques.default;
const reBaseUrl = exports.reBaseUrl = _sesatheques.default.reBaseUrl;
const addSesatheque = exports.addSesatheque = _sesatheques.default.addSesatheque;
const addSesatheques = exports.addSesatheques = _sesatheques.default.addSesatheques;
const exists = exports.exists = _sesatheques.default.exists;
const getBaseId = exports.getBaseId = _sesatheques.default.getBaseId;
const getBaseIdFromRessource = exports.getBaseIdFromRessource = _sesatheques.default.getBaseIdFromRessource;
const getBaseIdFromRid = exports.getBaseIdFromRid = _sesatheques.default.getBaseIdFromRid;
const getBaseIdFromUrlQcq = exports.getBaseIdFromUrlQcq = _sesatheques.default.getBaseIdFromUrlQcq;
const getBaseUrl = exports.getBaseUrl = _sesatheques.default.getBaseUrl;
const getComponents = exports.getComponents = _sesatheques.default.getComponents;
const getList = exports.getList = _sesatheques.default.getList;
const getRidComponents = exports.getRidComponents = _sesatheques.default.getRidComponents;
const defaultBaseId = exports.defaultBaseId = _sesatheques.default.defaultBaseId;
const defaultBaseUrl = exports.defaultBaseUrl = _sesatheques.default.defaultBaseUrl;