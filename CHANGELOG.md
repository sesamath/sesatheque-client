Changelog sesatheque-client
============================

1.2.x
=====
Changements importants sur la gestion des id, désormais les items ont tous un rid qui contient baseId

Toutes les méthodes du client deviennent des exports nommés d'un module séparé, que getClient renvoie après 
initialisation

Breaking changes
----------------
1) Le format de SesathequeItem change
- remove SesathequeItem.id
- remove SesathequeItem.baseId
- remove SesathequeItem.baseNameAlias
- add SesathequeItem.rid
- add SesathequeItem.aliasRid
- getItem(baseId, id, next) devient getItem(rid, next) 

2) le format des sésathèques à passer à sesatheque-client:getClient change, il faut désormais un tableau de {baseId: 
string, baseUrl: string}

Autres changements
------------------

1.1.x
=====
Grandes lignes
- Les sources passent en es6 dans src/, le module es5 est dans dist/
- ajout du module fetch pour ceux qui veulent simplement récupérer des ressources sur une sésathèque

1.1.1
-----
- le module sesatheques passe en global pour simuler un singleton

1.1.2
-----
- ajout sesatheques.getBaseIdFromId
